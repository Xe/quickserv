# quickserv

[![built with
nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org) [![Build
Status](https://drone.tulpa.dev/api/badges/Xe/quickserv/status.svg)](https://drone.tulpa.dev/Xe/quickserv)
[![Written in Rust](https://img.shields.io/badge/written%20in-Rust-orange)](https://www.rust-lang.org/)

A quick HTTP server for when you have given up on life.

`quickserv` serves `.` on port `3000`. Customizable with flags:

```
quickserv 0.1.0
A quick HTTP fileserver

USAGE:
    quickserv [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --dir <dir>       [env: DIR=]  [default: .]
    -p, --port <port>     [env: PORT=]  [default: 3000]
```

## Docker

```console
$ nix-build docker.nix
$ docker load -f result
$ docker push xena/quickserv
```

## Release

- Bump the version in `Cargo.toml`
- Rerun CI
- Tag a new release
