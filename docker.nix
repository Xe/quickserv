{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { };
  callPackage = pkgs.lib.callPackageWith pkgs;
  quickserv = callPackage ./default.nix { };

  dockerImage = pkg:
    pkgs.dockerTools.buildLayeredImage {
      name = "xena/quickserv";
      tag = "${quickserv.version}";

      contents = [ pkg ];

      config = {
        Cmd = [ "/bin/quickserv" ];
        WorkingDir = "/";
      };
    };

in dockerImage quickserv
