let pkgs = import <nixpkgs> { };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    # go
    go
    goimports
    nur.repos.xe.gopls
    vgo2nix

    # rust
    cargo
    cargo-watch
    rls
    rustc
    rustfmt
  ];
}
