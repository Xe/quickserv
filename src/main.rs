use std::path::PathBuf;
use structopt::StructOpt;

/// A quick HTTP fileserver
#[derive(StructOpt, Debug)]
#[structopt(name = "quickserv")]
struct Opt {
    // The HTTP port to listen on
    #[structopt(short, long, env="PORT", default_value="3000")]
    port: u16,

    // The path to serve
    #[structopt(short, long, env="DIR", default_value=".")]
    dir: PathBuf,
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let opt = Opt::from_args();
    log::info!("serving {:?} on port {}", opt.dir, opt.port);

    warp::serve(warp::fs::dir(opt.dir))
        .run(([0, 0, 0, 0], opt.port))
        .await;
}
